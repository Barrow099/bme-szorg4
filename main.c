/*
*  Tisztelt javito. 
* Nem teljesen ertem az elozo elutasitas indokanak egy reszet. Mit ert az alatt, hogy a program ne szalljon el ervenytelen bemenet eseten
* es ne lehessen tulindexelni? Az en megoldasom (elvileg ellenorzi a programban a loopok ervenyesseget, tulindexeles eseten is hibauzenettel megszakitja a 
* futtatas. Ha en ertelmeztem a feladatot felre, kerem tudassa velem, hogy mi lenne a helyes ut. 
* Koszonom szepen.
* Magyar Mate
*
* P.S.: Elnezest az ekezetes betuk hianyaert, angol billentyuzetem van. :)
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct BF_Data {
    const char *program;
    char memory[32786];
    int mem_pos;
    int prog_pos;
} BF_Data;

void process_next(char, BF_Data *);

bool check_syntax(const char *program);

int main() {

    const char nev[] = "++++++++[>++++++++<-]>+++++++++++++.++++++++++++++++++++.+++++++++++++++++++.---------------.";
    BF_Data runtime_data = {.prog_pos = 0, .mem_pos = 0, .program = nev, .memory = {0}};

    if (!check_syntax(runtime_data.program)) {
        return -1;
    }

    while (runtime_data.program[runtime_data.prog_pos] != 0) {
        char command = runtime_data.program[runtime_data.prog_pos];
        process_next(command, &runtime_data);
        runtime_data.prog_pos++;
    }
    return 0;
}

bool check_syntax(const char *program) {
    int slcount = 0;
    int elcount = 0;
    int pos = 0;
    while (program[pos] != 0) {
        if (program[pos] == '[') {
            slcount++;
        } else if (program[pos] == ']') {
            if (slcount < elcount + 1) {
                printf("Syntax error at %d\n", pos);
                printf("Loop closed before started\n");
                return false;
            } else {
                elcount++;
            }
        }
        pos++;
    }    if (elcount != slcount) {
        printf("Syntax error in the program\nThere are unclosed loop(s)");
        return false;
    }
    return true;
}

void process_next(char command, BF_Data *runtime) {
    //BF_Data runtime = *rt;
    switch (command) {
        case '+':
            runtime->memory[runtime->mem_pos] += 1;
            break;
        case '-':
            runtime->memory[runtime->mem_pos] -= 1;
            break;
        case '.':
            printf("%c", runtime->memory[runtime->mem_pos]);
            break;
        case ',': {
            if (scanf("%c", &runtime->memory[runtime->mem_pos]) == EOF) {
                runtime->memory[runtime->mem_pos] = -1;
            }
        };
            break;
        case '<':
            runtime->mem_pos--;
            break;
        case '>':
            runtime->mem_pos++;
            break;
        case '[': {
            int start = runtime->prog_pos + 1;
            int end = 0;
            int skipcount = 0;
            for (int pos = start; end == 0 && runtime->program[pos] != 0; pos++) {
                if (runtime->program[pos] == ']') {
                    if (skipcount > 0)
                        skipcount--;
                    else
                        end = pos;
                } else if (runtime->program[pos] == '[') {
                    skipcount++;
                }
            }

            while (runtime->memory[runtime->mem_pos] != 0) {

                runtime->prog_pos = start;
                while (runtime->prog_pos < end) {
                    process_next(runtime->program[runtime->prog_pos], runtime);
                    runtime->prog_pos++;
                }
            }
            runtime->prog_pos = end;
        }
            break;
        default: //Comment
            break;
    }

    if (runtime->mem_pos < 0 || runtime->mem_pos > 32786) {
        printf("Ervenytelen pozicio: %d\n", runtime->mem_pos);
        exit(-1);
    }

}


